using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    
    [SerializeField] public float SideScreenThreshold = 0.1f;
    [SerializeField] public float Speed = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        {
            Vector3 Right2D = Vector3.ProjectOnPlane(transform.right, new Vector3(0f, 1f, 0f));
            Vector3 Forward2D = Vector3.ProjectOnPlane(transform.forward, new Vector3(0f, 1f, 0f));
            
            if (mousePos.x / Screen.width < SideScreenThreshold)
            {
                transform.position -= Right2D * Speed;
            }
            
            if (mousePos.x / Screen.width > 1f - SideScreenThreshold)
            {
                transform.position += Right2D * Speed;
            }
            
            if (mousePos.y / Screen.height < SideScreenThreshold)
            {
                transform.position -= Forward2D * Speed;
            }
            
            if (mousePos.y / Screen.height > 1f - SideScreenThreshold)
            {
                transform.position += Forward2D * Speed;
            }
        }
    }
}
