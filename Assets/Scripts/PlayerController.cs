using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField] protected float RayCastDistance = 100f;
    
    private Unit SelectedUnit;
    
    // Listen to mouse inputs
    void HandleMouseInputs()
    {
        // Left Click
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Assert(Camera.main != null, "Camera.main != null");
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            bool bNoValid = true;
            if (Physics.Raycast(ray, out var hit, RayCastDistance))
            {
                var hitUnit = hit.collider.GetComponent<Unit>();
                if (hitUnit != null)
                {
                    bNoValid = false;
                    
                    SelectedUnit = hitUnit;
                    SelectedUnit.Select();
                }    
            }
            
            if (bNoValid && SelectedUnit != null)
            {
                SelectedUnit.Deselect();
                SelectedUnit = null;
            } 
        }

        // Right Click
        if (Input.GetMouseButtonDown(1))
        {
            if (!SelectedUnit) return;
            
            Debug.Assert(Camera.main != null, "Camera.main != null");
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            if (!Physics.Raycast(ray, out var hit, RayCastDistance)) return;
            if (!hit.collider.CompareTag("Ground")) return;
            
            SelectedUnit.MoveTo(hit.point);
        }
    }

    // Update is called once per frame
    void Update()
    {
        HandleMouseInputs();
    }
}
