using System;
using System.Collections;
using System.Collections.Generic;
using MiscUtil.Collections.Extensions;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.Rendering;

public class CandleLightController : MonoBehaviour
{
    public Light Light;
    public SphereCollider Sphere;
    public TextMesh Text;

    public float MinLightVal = 1.0f;
    private float CurrentLightVal = 1.0f;

    private List<CandleLightController> LightsInRange;
    
    // Start is called before the first frame update
    void Start()
    {
        LightsInRange = new List<CandleLightController>();

        UpdateLightVal(MinLightVal);
    }

    private void OnValidate()
    {
        UpdateLightVal(MinLightVal);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateLightsInRange();
    }

    private void UpdateLightsInRange()
    {
        float LightVal = 1.0f;
        Vector3 Location = transform.position;
        foreach (var Light in LightsInRange)
        {
            Vector3 OtherPosition = Light.GameObject().transform.position;
            float Distance = (Location - OtherPosition).magnitude;

            float AddVal = Mathf.Clamp(1.0f - Distance, 0.0f, 1.0f);
            LightVal += AddVal;
        }

        UpdateLightVal(LightVal);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Light"))
        {
            CandleLightController OtherLight = other.GetComponent<CandleLightController>();
            LightsInRange.Add(OtherLight);
        }
            
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Light"))
        {
            CandleLightController OtherLight = other.gameObject.GetComponent<CandleLightController>();
            LightsInRange.Remove(OtherLight);
        }
    }

    void UpdateLightVal(float lightValue)
    {
        CurrentLightVal = lightValue;
        Light.range = lightValue;
        Sphere.radius = lightValue;
        Text.text = lightValue.ToString();
    }
}
