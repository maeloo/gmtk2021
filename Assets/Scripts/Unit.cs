using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Unit : MonoBehaviour
{
    [SerializeField] private Material _DefaultMat;
    [SerializeField] private Material _SelectedMat;
    
    protected MeshRenderer _MeshRenderer;
    protected NavMeshAgent _NavAgent;

    private void Start()
    {
        _MeshRenderer = GetComponent<MeshRenderer>();
        _NavAgent = GetComponent<NavMeshAgent>();
    }

    public void Select()
    {
        _MeshRenderer.material = _SelectedMat;
    }
    
    public void Deselect()
    {
        _MeshRenderer.material = _DefaultMat;
    }
    
    public void MoveTo(Vector3 Location)
    {
        _NavAgent.destination = Location;
    }
}
